function _simpelads_extend_load(elem, tid, num, path, img_loader) {
  (function ($) {
    basepath = Drupal.settings.basePath;
    if (tid > 0 && num > 0) {
      if (img_loader != '')
        $(elem).html(img_loader);
      	$.get(basepath + 'simpleads-extend/load/' + tid + '/' + num + '?simpleads-extend-path=' + path, function (data) {
        $(elem).html(data);
      });
    }
  }(jQuery));
}